import bs4
import subprocess
from entity import Entity
import datetime

text_filename = '/tmp/text.txt'

def timex3_to_entity(tags):
    entity = Entity()
    for tag in tags:
        if tag['type'] == "DATE":
            vals = tag['value'].split("-")
            for val, field in zip(vals, ['year', 'month', 'day']):
                entity.__setattr__(field, int(val))
        elif tag['type'] == "TIME":
            time_string = tag['value'].split('T')[-1]
            vals = time_string.split(':')
            for val, field in zip(vals, ['hour', 'minute']):
                entity.__setattr__(field, int(val))
        elif tag['type'] == "DURATION":
            pass
            # value = tag.value
            # value = value[1:]
            # if value[0] == 'T':
            #     pass
            # else:
            #     unit = value[-1]
            #     value = value[:-1]
            #     if unit == "D":
            #         entity.day = datetime.now().timetuple().yr_days
    return entity


def parse(text):
    with open(text_filename, 'w') as f:
        f.write(text)
    
    result = subprocess.run(['java', '-jar', 'heideltime.jar', '-t', 'news', '-l', 'russian', text_filename],
        stdout=subprocess.PIPE)
    output_text = result.stdout.decode()

    soup = bs4.BeautifulSoup(output_text, 'xml')
    tags = soup.find_all('TIMEX3')

    entity = timex3_to_entity(tags)
    return entity.to_json()
