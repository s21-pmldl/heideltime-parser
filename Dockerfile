FROM openjdk:17-slim
WORKDIR /app

RUN apt-get update && apt-get install -y --no-install-recommends \
    python3.7 \
    python3-pip \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY treetagger ./treetagger

COPY heideltime.jar .
COPY config.props .

COPY requirements.txt .
RUN pip3 install -qr requirements.txt

COPY parser.py .
COPY entity.py .
COPY app.py .
COPY text.txt .

ENTRYPOINT [ "python3", "app.py" ]
