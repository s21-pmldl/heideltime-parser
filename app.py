from flask import Flask, make_response
from flask.globals import request
from parser import parse

app = Flask(__name__)


@app.route('/parse', methods=['GET'])
def parse_endpoint():

    if 'text' not in request.json:
        return make_response("request json must contain 'text' field", 400)

    input_text = request.json['text']
    output_text = parse(input_text)

    return output_text


app.run(host='0.0.0.0', port=3000, debug=True)
